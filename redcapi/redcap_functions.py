import requests
import xml.etree.ElementTree as ET

class REDCap:
    def __init__(self, url=None, token=None, cert_location=None):
        """url will be something like 'https://redcap.myschool.edu/redcap/api/'. token
        will be something like 'F4C42590067C3DE18DE53EAA287D28C1'.

        cert_location is the path to cert_chain.pem.  It only needs to be
        specified if OpenSSL can't automatically verify the server cert,
        e.g. due to the server not providing the full chain.
        """
        self.url = url
        self.token = token
        self.verify = cert_location if cert_location is not None else True

    def post(self, data):
        """Post data (a dict) to self.url and return the response if OK (otherwise raise
        an exception).
        """
        data['token'] = self.token
        r = requests.post(self.url, data=data, verify=self.verify)
        if r.status_code == 200:
            return r.text
        # errtree = ET.fromstring(r.text)
        # errmsg = ET.tostring(errtree, encoding='utf8', method='text').decode('utf-8')
        # raise RuntimeError(errmsg)
        raise RuntimeError(r.text)

    def get_redcap_version(self, inst_format='xml'):
        """This method returns the current REDCap version number as plain text (e.g.,
        4.13.18, 5.12.2, 6.0.0).

        Keyword arguments:
        inst_format -- csv, json, or xml (default xml)
        """
        data = {
            'content': 'version',
            'format':  inst_format,
        }
        return self.post(data)

    def get_project_info(self, inst_format='xml'):
        """This method allows you to export some of the basic attributes of a given
        REDCap project, such as the project's title, if it is longitudinal, if
        surveys are enabled, the time the project was created and moved to
        production, etc.

        The following attributes will be returned:
          project_id, project_title, creation_time, production_time,
          in_production, project_language, purpose, purpose_other, project_notes,
          custom_record_label, secondary_unique_field, is_longitudinal,
          surveys_enabled, scheduling_enabled, record_autonumbering_enabled,
          randomization_enabled, ddp_enabled, project_irb_number,
          project_grant_number, project_pi_firstname, project_pi_lastname,
          display_today_now_button

        Keyword arguments:
        inst_format -- csv, json, or xml (default xml)
        """
        data = {
            'content': 'project',
            'format':  inst_format,
        }
        return self.post(data)

    def get_metadata(self, inst_format='xml', fields=[]):
        """This method allows you to export the metadata for a project.

        Keyword arguments:
        inst_format -- csv, json, or xml (default xml)
        """
        data = {
            'content': 'metadata',
            'format':  inst_format,
            'fields': ','.join(fields),
            # 'forms': TODO,
            # 'returnFormat': TODO,
        }
        return self.post(data)

    def get_instruments(self, inst_format='xml'):
        """This method allows you to export a list of the data collection instruments
        for a project. This includes their unique instrument name as seen in the
        second column of the Data Dictionary, as well as each instrument's
        corresponding instrument label, which is seen on a project's left-hand menu
        when entering data. The instruments will be ordered according to their order
        in the project.

        Keyword arguments:
        inst_format -- csv, json, or xml (default xml)
        """
        data = {
            'content': 'instrument',
            'format':  inst_format,
        }
        return self.post(data)

    def get_records(self, rec_format='xml', rec_type='flat', records='', fields=[],
                    forms=[], events=[], survey_fields=True, radio_labels=False ):
        """This method allows you to export a set of records for a project.

        Keyword arguments:
        rec_format -- csv, json, xml, or odm (default xml)
        rec_type   -- flat or eav (default flat)
        records    -- list of record names to pull (default all)
        fields     -- list of field names to pull (default all)
        forms      -- list of forms to pull (default all)
        events     -- list of unique event names to pull (only for longitudinal projects)
        survey_fields -- include survey IDs and timestamps (default True)
        radio_labels  -- get labels of radio buttons rather than raw field names
        """
        for i,form in enumerate(forms):
            forms[i] = form.replace(' ','_')
        data = {
            'content':            'record',
            'format':             rec_format,
            'type':               rec_type,
            'records':            records,
            'fields':             ','.join(fields),
            'forms':              forms,  # TODO?: should be comma sep. string not list
            'events':             ','.join(events),
            'rawOrLabel':         'label' if radio_labels else 'raw',
            # 'rawOrLabelHeaders':  'label' if TODO else 'raw',
            'exportSurveyFields': str(survey_fields).lower(),
        }
        return self.post(data)

    def add_records(self, data, rec_format='xml', rec_type='flat',
                    overwrite_behavior='normal', force_auto_number=False):
        """This method allows you to import a set of records for a project.

        Keyword arguments:
        data               -- the data to import into REDCap
        rec_format         -- csv, json, xml, or odm (default xml)
        rec_type           -- flat or eav (default flat)
        overwrite_behavior -- 'normal', or 'overwrite' if NULLs should overwrite existing data
        force_auto_number  -- ignore record numbers in data; let REDCap assign them
        """
        data = {
            'content':           'record',
            'format':            rec_format,
            'type':              rec_type,
            'overwriteBehavior': overwrite_behavior,
            'forceAutoNumber':   str(force_auto_number).lower(),
            'data':              data,
            # 'dateFormat':     # MDY, DMY, or YMD
            # 'returnContent':  # count [default], ids, or auto_ids
            # 'returnFormat':   # csv, json, or xml
        }
        return self.post(data)

    def get_survey_link(self, record, instrument, event='', repeat_instance=1):
        """This method returns a unique survey link (i.e., a URL) in plain text format
        for a specified record and data collection instrument (and event, if
        longitudinal) in a project. If the user does not have 'Manage Survey
        Participants' privileges, they will not be able to use this method, and None
        will be returned. If the specified data collection instrument has not been
        enabled as a survey in the project, None will be returned.  If
        repeat_instance doesn't already exist, a new instance with this value will
        be created.
        """
        data = {
            'content':         'surveyLink',
            'record':          record,
            'instrument':      instrument,
            'event':           event,
            'repeat_instance': repeat_instance,
        }
        return self.post(data)

    def get_survey_return_code(self, record, instrument, event='', repeat_instance=1):
        """This method returns a unique Return Code in plain text format for a specified
        record and data collection instrument (and event, if longitudinal) in a
        project. If the user does not have 'Manage Survey Participants' privileges,
        they will not be able to use this method, and an error will be returned. If
        the specified data collection instrument has not been enabled as a survey in
        the project or does not have the 'Save & Return Later' feature enabled, an
        error will be returned.
        """
        data = {
            'content':         'surveyReturnCode',
            'record':          record,
            'instrument':      instrument,
            'event':           event,
            'repeat_instance': repeat_instance,
        }
        return self.post(data)

################################################################################

# TODO:
# - print debug statements?
# - other functions, see https://redcap.urmc.rochester.edu/redcap/api/help/

################################################################################
