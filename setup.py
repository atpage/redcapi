from setuptools import setup

setup( name='redcapi',
       version='2018.07.06',
       description='A library to interface with REDCap via the API',
       url='https://bitbucket.org/atpage/redcapi/',
       author='Alex Page',
       author_email='alex.page@rochester.edu',
       license='MIT',
       packages=['redcapi'],
       install_requires=['requests'],
       keywords='REDCap',
       zip_safe=False )
