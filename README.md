# REDCAPI #

A REDCap API wrapper for Python.

### What is this repository for? ###

For now, accessing basic data (e.g. survey responses) from REDCap using Python.  
I may eventually build it out to cover the complete API.

### How do I get set up? ###

My preferred method:

    git clone git@bitbucket.org:atpage/redcapi.git
    cd redcapi
    sudo pip3 install -e .

### Usage example ###

To print the REDCap version number:

    from redcapi import REDCap
    my_redcap = REDCap(url='https://redcap.myschool.edu/redcap/api/',
                       token='F4C42590067C3DE18DE53EAA287D28C1')
    print( my_redcap.get_redcap_version() )

### Who do I talk to? ###

* Alex Page, alex.page@rochester.edu